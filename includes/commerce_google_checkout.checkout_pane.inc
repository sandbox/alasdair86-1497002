<?php

/**
 * @file
 * Checkout pane callback functions for the commerce_google_checkout module.
 */


/**
 * Checkout pane callback: returns a google checkout payment pane.
 */
function commerce_google_checkout_review_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form['submit'] = array(
    '#button_type' => 'image_button',
    '#type' => 'image_button',
    '#src' => '/' . drupal_get_path('module', 'commerce_google_checkout') . '/images/checkout_button.gif',
    '#value' => t('Proceed to Google checkout'),
    '#submit' => array('commerce_google_checkout_form_submit'),
  );

  return $pane_form;
}
