Google Checkout Payment Gateway
----------------------
A Payment gateway for Google Checkout.  To use this module you will need to have
an account set up with Google Checkout and have received a merchant key as well
as a merchant ID.  There is a sandbox option you can use.

Installation
------------
 - Download and enable the module
 - Go to the payment methods settings page at
 admin/commerce/config/payment-methods
 - Click the edit on the "Commerce Google Checkout" Rule and then again on the
 action for the rule.
 - Insert secret/merchant Id and decide whether to use 3D secure or not.
 - Create a google checkout seller account.
 - In settings/integration add in the API callback url of your site and also use
 API version 2.5

Author
------
Alasdair Cowie-Fraser
alasdair.cowie-fraser@codeenigma.com

Future Works
------------
I will integrate the tax rules as well as well as creating a payment terminal
for remotely charging customers as well as being able to do remote refunds.
