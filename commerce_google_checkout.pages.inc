<?php

/**
 * @file
 * Callback functions to handle responses from Google Checkout.
 */

/**
 * Handles Google Checkout payment notification callback.
 */
function commerce_google_checkout_callback() {
  $serial_number = $_POST['serial-number'];
  if (isset($serial_number)) {
    $xml = '<notification-history-request xmlns="http://checkout.google.com/schema/2">
          <serial-number>' . check_plain($serial_number) . '</serial-number>
          </notification-history-request>';

    if ($response = commerce_google_checkout_send_request('reports', $xml)) {
      $response_array = json_decode(json_encode((array) simplexml_load_string($response->asXML())), 1);
      watchdog('commerce google checkout', 'Notification document: @xml', array('@xml' => $response->asXML()), WATCHDOG_DEBUG);

      switch ($response->getName()) {
        case 'new-order-notification':
          $success = commerce_google_checkout_new_order($response_array);
          break;

        case 'order-state-change-notification':
          $success = commerce_google_checkout_order_state_change($response_array);
          break;

        case 'risk-information-notification':
          $success = commerce_google_checkout_accept_risk($response_array);
          break;

        case 'authorization-amount-notification':
          $success = commerce_google_checkout_authorize_amount($response_array);
          break;

        case 'charge-amount-notification':
          $success = commerce_google_checkout_charge_order($response_array);
          break;

        default:
          $success = TRUE;
          break;
      }

      if ($success) {
        commerce_google_checkout_notification_acknowledgement(check_plain($serial_number));
      }
      else {
        commerce_google_checkout_notification_error();
      }

    }
  }
  return MENU_NOT_FOUND;
}
